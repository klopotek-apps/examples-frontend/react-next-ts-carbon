/**
 * Theme types for the Carbon Components (page or component-specific)
 */
export type Ttheme = "white" | "g10" | "g90" | "g100";
