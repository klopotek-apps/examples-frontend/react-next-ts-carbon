import React from "react";
import {
  DataTable,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableExpandHeader,
  TableHeader,
  TableBody,
  TableExpandRow,
  TableCell,
  TableExpandedRow,
  DataTableRow,
  DataTableHeader,
} from "@carbon/react";


type TRepoTableProps = {
  rows: DataTableRow<any>[];
  headers: DataTableHeader[];
};


// TODO: resolve TS conflicts due to empty initial rows and headers properties
/**
 * Component rendering a table with the list of the repositories.
 * @param param0 
 * @returns 
 */
const RepoTable: React.FC<TRepoTableProps> = ({ rows, headers }) => {
  /**
   * Lookup function to create a proper description of the repository.
   * @param rowId
   * @returns
   */
  const getRowDescription = (rowId: string) => {
    const row = rows.find(({ id }) => id === rowId);
    return row ? row.description : "";
  };

  return (
    <DataTable
      rows={rows}
      headers={headers}
      render={({
        rows,
        headers,
        getHeaderProps,
        getRowProps,
        getTableProps,
      }) => (
        <TableContainer
          title="Carbon Repositories"
          description="A collection of public Carbon repositories."
        >
          <Table {...getTableProps()}>
            <TableHead>
              <TableRow>
                <TableExpandHeader />
                {headers.map((header) => (
                  <TableHeader  {...getHeaderProps({ header })}>
                    {header.header}
                  </TableHeader>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <React.Fragment key={row.id}>
                  <TableExpandRow {...getRowProps({ row })}>
                    {row.cells.map((cell) => (
                      <TableCell key={cell.id}>{cell.value}</TableCell>
                    ))}
                  </TableExpandRow>
                  <TableExpandedRow colSpan={headers.length + 1}>
                  <p>{getRowDescription(row.id)}</p>
                  </TableExpandedRow>
                </React.Fragment>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    />
  );
};

export default RepoTable;
