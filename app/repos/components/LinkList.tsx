import { Link, Grid, Column } from "@carbon/react";

type TLinkListProps = {
  url: string;
  homepageUrl: string;
};

/**
 * Component for creating a comma-separated list of repository and home page links. returns an unordered list.
 * If the repository does not have a home page URL, only render the repository link.
 * @param TLinkListProps
 * @returns
 */
const LinkList: React.FC<TLinkListProps> = ({ url, homepageUrl }) => {
  return (
    <ul style={{ display: "flex" }}>
      <li>
        <Link href={url}>GitHub</Link>
      </li>
      {homepageUrl && (
        <li>
          <span>&nbsp;|&nbsp;</span>
          <Link href={homepageUrl}>Homepage</Link>
        </li>
      )}
    </ul>
  );
};

export default LinkList;
