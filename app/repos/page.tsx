"use client";
import React, { useEffect, useState } from "react";
import {
  Grid,
  Column,
  DataTableSkeleton,
  Content,
  Pagination,
} from "@carbon/react";
import RepoTable from "./components/RepoTable";
import LinkList from "./components/LinkList";
import getRepos from "../actions/getRepos";

const headers = [
  {
    key: "name",
    header: "Name",
  },
  {
    key: "createdAt",
    header: "Created",
  },
  {
    key: "updatedAt",
    header: "Updated",
  },
  {
    key: "issueCount",
    header: "Open Issues",
  },
  {
    key: "stars",
    header: "Stars",
  },
  {
    key: "links",
    header: "Links",
  },
];

/**
 * Formats data coming from the API endpoint
 * @param rows
 * @returns
 */
const getRowItems = (rows) =>
  rows.map((row) => ({
    ...row,
    key: row.id,
    stars: row.stargazers_count,
    issueCount: row.open_issues_count,
    createdAt: new Date(row.created_at).toLocaleDateString(),
    updatedAt: new Date(row.updated_at).toLocaleDateString(),
    links: <LinkList url={row.html_url} homepageUrl={row.homepage} />,
  }));

const Repos: React.FC = (): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>("");
  const [rows, setRows] = useState([]);

  // for pagination
  const [firstRowIndex, setFirstRowIndex] = useState(0);
  const [currentPageSize, setCurrentPageSize] = useState(10);

  //  TODO: replace useEffect with back-end actions (SSR)
  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      const _rows = await getRepos();
      if (!_rows) setError("Error obtaining repository data");

      setRows(getRowItems(_rows));
    }

    fetchData();
    setLoading(false);
  }, []);

  if (loading) {
    return (
      <Grid className="repo-page">
        <Column lg={16} md={8} sm={4} className="repo-page__r1">
          <DataTableSkeleton
            columnCount={headers.length + 1}
            rowCount={10}
            headers={headers}
          />
        </Column>
      </Grid>
    );
  }

  if (error) {
    return (
      <Content>
        <div>`Error! ${error}`</div>
      </Content>
    );
  }

  return (
    <Content>
      <Grid className="repo-page">
        <Column lg={16} className="repo-page__r1">
          <RepoTable
            headers={headers}
            rows={rows.slice(firstRowIndex, firstRowIndex + currentPageSize)}
          />
          <Pagination
            totalItems={rows.length}
            backwardText="Previous page"
            forwardText="Next page"
            pageSize={currentPageSize}
            pageSizes={[5, 10, 15, 25]}
            itemsPerPageText="Items per page"
            onChange={({
              page,
              pageSize,
            }: {
              page: number;
              pageSize: number;
            }): void => {
              if (pageSize !== currentPageSize) {
                setCurrentPageSize(pageSize);
              }
              setFirstRowIndex(pageSize * (page - 1));
            }}
          />
        </Column>
      </Grid>
    </Content>
  );
};

export default Repos;
