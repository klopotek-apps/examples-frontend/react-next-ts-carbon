/**
 * Helper function to retrieve the repository data from the endpoint
 * @returns
 */
async function getRepos(): Promise<[] | null> {
  const res = await fetch(`/api/repos`, { method: "GET" });

  const _data = await res.json();
  //   console.log("getRepos: ", data);
  if (res.status == 200) {
    return _data;
  }

  return null;
}

export default getRepos;
