import React from "react";
import { Grid, Column } from "@carbon/react";

/**
 * Take in a phrase and separate the third word in an array
 * @param phrase
 * @returns
 */
function createArrayFromPhrase(phrase: string) {
  const splitPhrase = phrase.split(" ");
  const thirdWord = splitPhrase.pop();
  return [splitPhrase.join(" "), thirdWord];
}

type TInfoCardProps = {
  heading: string;
  body: React.ReactNode;
  icon: any;
};

const InfoCard: React.FC<TInfoCardProps> = (props) => {
  const splitHeading = createArrayFromPhrase(props.heading);
  return (
    <Column sm={4} md={8} lg={4} className="info-card">
      <h4 className="info-card__heading">
        {`${splitHeading[0]} `}
        <strong>{splitHeading[1]}</strong>
      </h4>
      <p className="info-card__body">{props.body}</p>
      {props.icon()}
    </Column>
  );
};

export { InfoCard };
