import React from 'react';
import { Grid, Column } from '@carbon/react';

type TInfoSectionProps={
  className: string,
  heading: string,
  children: React.ReactNode
}

const InfoSection: React.FC<TInfoSectionProps> = (props): JSX.Element => (
  <Grid className={`${props.className} info-section`}>
    <Column md={8} lg={4} xlg={3}>
      <h3 className="info-section__heading">{props.heading}</h3>
    </Column>
    {props.children}
  </Grid>
);

export {InfoSection};