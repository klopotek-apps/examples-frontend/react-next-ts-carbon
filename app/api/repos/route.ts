import { NextRequest, NextResponse } from "next/server";
import { Octokit } from "@octokit/core";

type TResponseData = any;

export async function GET(
  req: NextRequest,
): Promise<NextResponse<TResponseData>> {
  //   console.log("GET /repos/");

  const octokitClient = new Octokit({});

  const res = await octokitClient.request("GET /orgs/{org}/repos", {
    org: "carbon-design-system",
    per_page: 75,
    sort: "updated",
    direction: "desc",
  });

  if (res.status === 200) {
    // console.log("GET /repos/: ", res.data);
    return NextResponse.json(res.data, { status: res.status });
  } else {
    console.error("GET /repos/: ", "Error obtaining repository data");
    return NextResponse.json([], { status: res.status });
  }
}
