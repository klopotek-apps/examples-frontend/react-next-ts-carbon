# Next.js with TypeScript and Carbon Design System

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) to showcase the utilization of IBM's Carbon Design System for React applications. The example based on the [tutorial](https://carbondesignsystem.com/developing/react-tutorial/overview) for creating a React app with the Carbon Design System.

The utilized Carbon packages:

- carbon-components - component styles
- carbon-components-react - React components
- @carbon/icons-react - React icons

In addtion, the Sass is introduced as the Carbon styles are authored in Sass. Next.js has built-in support for Sass using both the `.scss` and `.sass` extensions.

## Layout

### Standard size

![WebsiteLayout-1](./react-next-ts-carbon-layout-1.png)

![WebsiteLayout-2](./react-next-ts-carbon-layout-2.png)

## Available comands

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
